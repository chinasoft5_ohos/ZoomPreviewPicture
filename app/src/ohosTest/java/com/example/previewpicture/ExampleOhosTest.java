/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.previewpicture;

import com.example.previewpicture.bean.UserViewInfo;
import com.previewlibrary.GPreviewBuilder;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * ExampleOhosTest.
 *
 * @author author
 * @version version
 */
public class ExampleOhosTest {
    /**
     * testGetUrls.
     */
    @Test
    public void testGetUrls() {
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        List<String> urls = ImageUrlConfig.getUrls(currentTopAbility);
    }

    /**
     * testGetGifUrls.
     */
    @Test
    public void testGetGifUrls() {
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        List<UserViewInfo> urls = ImageUrlConfig.getGifUrls(currentTopAbility);
    }

    /**
     * testGetVideos.
     */
    @Test
    public void testGetVideos() {
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        List<UserViewInfo> urls = ImageUrlConfig.getVideos(currentTopAbility);
    }

    /**
     * testGPreviewBuilder.
     */
    @Test
    public void testGPreviewBuilder() {
        Ability currentTopAbility = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility();
        List<String> urls = ImageUrlConfig.getUrls(currentTopAbility);
        ArrayList<UserViewInfo> mThumbViewInfoList = new ArrayList<>();
        for (int i = 0; i < urls.size(); i++) {
            mThumbViewInfoList.add(new UserViewInfo(urls.get(i)));
        }
        GPreviewBuilder.from(currentTopAbility)
                .setData(mThumbViewInfoList)
                .setCurrentIndex(0)
                .isDisableDrag(true, 0.6f)
                .setType(GPreviewBuilder.IndicatorType.Number)
                .start();
    }
}