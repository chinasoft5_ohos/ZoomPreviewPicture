package com.example.previewpicture.pager;

import com.example.previewpicture.ImageUrlConfig;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.bean.UserViewInfo;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;

import java.util.ArrayList;
import java.util.List;

/**
 * ViewPagerAbility.
 *
 * @author author
 * @version version
 */
public class ViewPagerAbility extends Ability {
    private ArrayList<UserViewInfo> mThumbViewInfoList = new ArrayList<>();
    private PageSlider mViewPager;
    private MyPagerAdaper adapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_view_pager);
        int color = getColor(ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(color);

        mViewPager = (PageSlider) findComponentById(ResourceTable.Id_mViewPager);
        //准备数据
        List<String> urls = ImageUrlConfig.getUrls(this);
        for (int i = 0; i < 1; i++) {
            mThumbViewInfoList.add(new UserViewInfo(urls.get(i)));
        }
        adapter = new MyPagerAdaper(mThumbViewInfoList, this);
        mViewPager.setProvider(adapter);
    }
}
