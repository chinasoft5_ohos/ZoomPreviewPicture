package com.example.previewpicture.pager;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.app.GlideBitmapTransformation;
import com.example.previewpicture.bean.UserViewInfo;
import com.previewlibrary.GPreviewBuilder;

import ohos.aafwk.ability.Ability;
import ohos.agp.components.*;
import ohos.agp.utils.Rect;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * MyPagerAdaper.
 *
 * @author author
 * @version version
 */
public class MyPagerAdaper extends PageSliderProvider {
    private Context mContext;
    private List<UserViewInfo> mThumbViewInfoList = new ArrayList<>();

    public MyPagerAdaper(List<UserViewInfo> list, Context mContext) {
        this.mThumbViewInfoList = list;
        this.mContext = mContext;

    }

    @Override
    public int getCount() {
        return mThumbViewInfoList.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {

        Image view = (Image) LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_image2, null, false);
        Glide.with(mContext)
                .load(mThumbViewInfoList.get(position).getUrl())
                .apply(RequestOptions.bitmapTransform(new GlideBitmapTransformation())
                        .error(ResourceTable.Media_ic_iamge_zhanwei))
                .into(view);
        view.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setRect(component);
                GPreviewBuilder.from((Ability) mContext)
                        .setData(mThumbViewInfoList)
                        .setSingleShowType(false)
                        .setCurrentIndex(position)
                        .setType(GPreviewBuilder.IndicatorType.Dot)
                        .start();
            }
        });
        componentContainer.addComponent(view);


        return view;
    }

    private void setRect(Component view) {
        for (UserViewInfo ss : mThumbViewInfoList) {
            Rect bounds = new Rect();
            view.getSelfVisibleRect(bounds);
            ss.setBounds(bounds);
        }
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }
}
