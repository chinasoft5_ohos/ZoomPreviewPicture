package com.example.previewpicture.bean;

import com.previewlibrary.enitity.IThumbViewInfo;
import ohos.agp.utils.Rect;
import ohos.utils.Parcel;

/**
 * @author yangc
 * @date 2017/4/26
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated: 图片预览实体类
 **/
public class UserViewInfo implements IThumbViewInfo {

    private String url;  //图片地址
    private Rect mBounds; // 记录坐标
    private String user = "用户字段";
    private String videoUrl;

    public UserViewInfo() {

    }

    public UserViewInfo(String url) {
        this.url = url;
    }

    public UserViewInfo(String videoUrl, String url) {
        this.url = url;
        this.videoUrl = videoUrl;
    }

    /**
     * getUser.
     *
     * @return String
     */
    public String getUser() {
        return user;
    }

    /**
     * setUser .
     *
     * @param user user
     */
    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String getUrl() {//将你的图片地址字段返回
        return url;
    }

    /**
     * setUrl .
     *
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public Rect getBounds() {
        // 将你的图片显示坐标字段返回
        return mBounds;
    }

    @Override
    public String getVideoUrl() {
        return videoUrl;
    }

    /**
     * setBounds .
     *
     * @param bounds bounds
     */
    public void setBounds(Rect bounds) {
        mBounds = bounds;
    }

    /**
     * setVideoUrl .
     *
     * @param videoUrl videoUrl
     */
    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    /**
     * PRODUCER .
     */
    public static final Producer<UserViewInfo> PRODUCER = new Producer<UserViewInfo>() {
        @Override
        public UserViewInfo createFromParcel(Parcel source) {
            UserViewInfo userViewInfo = new UserViewInfo();
            userViewInfo.unmarshalling(source);
            return userViewInfo;
        }
    };

    @Override
    public boolean marshalling(Parcel dest) {
        dest.writeString(this.url);
        dest.writeSequenceable(this.mBounds);
        dest.writeString(this.user);
        dest.writeString(this.videoUrl);
        return true;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        this.url = parcel.readString();
        this.mBounds = new Rect();
        parcel.readSequenceable(this.mBounds);
        this.user = parcel.readString();
        this.videoUrl = parcel.readString();
        return true;
    }
}
