package com.example.previewpicture.list;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.previewpicture.ImageUrlConfig;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.app.GlideBitmapTransformation;
import com.example.previewpicture.bean.UserViewInfo;
import com.previewlibrary.GPreviewBuilder;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Rect;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * ListView2Ability.
 *
 * @author author
 * @version version
 */
public class ListView2Ability extends Ability {

    private ArrayList<UserViewInfo> mThumbViewInfoList = new ArrayList<>();
    private ListContainer listView;
    private MyListAdapter adapter;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_list_view2);
        int color = getColor(ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(color);
        listView = (ListContainer) findComponentById(ResourceTable.Id_listView);
        //准备数据
        List<String> urls = ImageUrlConfig.getUrls(this);
        for (int i = 0; i < urls.size(); i++) {
            mThumbViewInfoList.add(new UserViewInfo(urls.get(i)));
        }
        adapter = new MyListAdapter();
        listView.setItemProvider(adapter);
        listView.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                computeBoundsBackward(position);
                GPreviewBuilder.from(ListView2Ability.this)
                        .setData(mThumbViewInfoList)
                        .setCurrentIndex(position)
                        .isDisableDrag(true, 0.6f)
                        .setType(GPreviewBuilder.IndicatorType.Number)
                        .start();
            }
        });

    }


    private void computeBoundsBackward(int postion) {
        for (int i = 0; i < mThumbViewInfoList.size(); i++) {
            Component itemView = listView.getComponentAt(i);
            Rect bounds = new Rect();
            if (itemView != null) {
                Image thumbView = (Image) itemView.findComponentById(ResourceTable.Id_iv);
                thumbView.getSelfVisibleRect(bounds);
                int width = bounds.getWidth();
                int height = bounds.getHeight();
                int[] locationOnScreen = thumbView.getLocationOnScreen();
                int left = locationOnScreen[0];
                int top = locationOnScreen[1];
                bounds.set(left, top, left + width, top + height);
            }
            mThumbViewInfoList.get(i).setBounds(bounds);
        }
    }

    private class MyListAdapter extends BaseItemProvider {

        @Override
        public int getCount() {
            return mThumbViewInfoList.size();
        }

        @Override
        public Object getItem(int i) {
            return mThumbViewInfoList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
            ViewHolder viewHolder;
            if (component == null) {
                component = LayoutScatter.getInstance(ListView2Ability.this).parse(ResourceTable.Layout_item_image, null, false);
                viewHolder = new ViewHolder(component);
                component.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) component.getTag();
            }
            viewHolder.btnVideo.setVisibility(Component.HIDE);
            Glide.with(ListView2Ability.this)
                    .load(mThumbViewInfoList.get(position).getUrl())
                    .apply(RequestOptions.bitmapTransform(new GlideBitmapTransformation())
                            .error(ResourceTable.Media_ic_iamge_zhanwei))
                    .listener(new RequestListener<Element>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Element> target, boolean b) {
                            viewHolder.iv.setPixelMap(ResourceTable.Media_ic_iamge_zhanwei);
                            return true;
                        }

                        @Override
                        public boolean onResourceReady(Element element, Object o, Target<Element> target, DataSource dataSource, boolean b) {
                            return false;
                        }
                    })
                    .into(viewHolder.iv);
            return component;
        }
    }

    static class ViewHolder {
        Image iv;
        Image btnVideo;

        public ViewHolder(Component component) {
            this.iv = (Image) component.findComponentById(ResourceTable.Id_iv);
            this.btnVideo = (Image) component.findComponentById(ResourceTable.Id_btnVideo);
        }
    }
}
