package com.example.previewpicture.custom;

import com.example.previewpicture.ImageUrlConfig;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.bean.UserViewInfo;
import com.example.previewpicture.rec.EasyGridProvider;
import com.example.previewpicture.rec.MyBaseQuickAdapter;
import com.previewlibrary.GPreviewBuilder;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Rect;

import java.util.ArrayList;
import java.util.List;

/**
 * GridViewCustomAbility.
 *
 * @author author
 * @version version
 */
public class GridViewCustomAbility extends Ability {
    private ArrayList<UserViewInfo> mThumbViewInfoList = new ArrayList<>();
    private ListContainer mRecyclerView;
    private int type;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_grid_view_custom);
        int color = getColor(ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(color);
        type = intent.getIntParam("type", 0);
        initView();
    }

    private void initView() {
        //准备数据
        List<String> urls = ImageUrlConfig.getUrls(this);
        for (int i = 0; i < urls.size(); i++) {
            mThumbViewInfoList.add(new UserViewInfo(urls.get(i)));
        }
        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_listView);

        MyBaseQuickAdapter myBaseQuickAdapter = new MyBaseQuickAdapter(this);
        myBaseQuickAdapter.setData(mThumbViewInfoList);
        myBaseQuickAdapter.setNumColumns(2);
        myBaseQuickAdapter.setGridSpacing(0);
        mRecyclerView.setItemProvider(myBaseQuickAdapter);
        myBaseQuickAdapter.notifyDataChanged();
        onItemClick(myBaseQuickAdapter);
    }

    private void onItemClick(MyBaseQuickAdapter myBaseQuickAdapter) {
        myBaseQuickAdapter.setOnItemClickListener(new EasyGridProvider.OnItemClickListener() {
            @Override
            public void onItemClick(Component component, int position) {
                computeBoundsBackward(position);
                if (type == 0) {//自定义ability
                    GPreviewBuilder.from(GridViewCustomAbility.this)
                            .to(CustomAbility.class)
                            .setData(mThumbViewInfoList)
                            .setCurrentIndex(position)
                            .setType(GPreviewBuilder.IndicatorType.Dot)
                            .start();
                } else if (type == 1) {//自定义Fragment
                    GPreviewBuilder.from(GridViewCustomAbility.this)
                            .setData(mThumbViewInfoList)
                            .setCurrentIndex(position)
                            .setUserFragment(UserFragment.class)
                            .setType(GPreviewBuilder.IndicatorType.Dot)
                            .start();
                } else {
                    //自定义ability和Fragment
                    GPreviewBuilder.from(GridViewCustomAbility.this)
                            .to(CustomAbility.class)
                            .setData(mThumbViewInfoList)
                            .setUserFragment(UserFragment.class)
                            .setCurrentIndex(position)
                            .setSingleFling(false)
                            .isDisableDrag(false)
                            .setType(GPreviewBuilder.IndicatorType.Dot)
                            .start();
                }
            }
        });
    }

    private void computeBoundsBackward(int postion) {
        for (int i = 0; i < mThumbViewInfoList.size(); i++) {
            ComponentContainer itemView = (ComponentContainer) mRecyclerView.getComponentAt(i / 2);
            Rect bounds = new Rect();
            if (itemView != null) {
                Component component = itemView.getComponentAt(i % 2 == 0 ? 0 : 1);
                if (component != null) {
                    Image thumbView = (Image) component.findComponentById(ResourceTable.Id_iv);
                    thumbView.getSelfVisibleRect(bounds);
                    int width = bounds.getWidth();
                    int height = bounds.getHeight();
                    int[] locationOnScreen = thumbView.getLocationOnScreen();
                    int left = locationOnScreen[0];
                    int top = locationOnScreen[1];
                    bounds.set(left, top, left + width, top + height);
                }
            }
            mThumbViewInfoList.get(i).setBounds(bounds);
        }
    }
}
