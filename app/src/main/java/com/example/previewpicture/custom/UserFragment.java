package com.example.previewpicture.custom;

import com.example.previewpicture.bean.UserViewInfo;
import com.previewlibrary.ResourceTable;
import com.previewlibrary.utils.Toast;
import com.previewlibrary.view.BasePhotoFragment;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

/**
 * author  yangc
 * date 2017/11/22
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated:
 */
public class UserFragment extends BasePhotoFragment {
    /****用户具体数据模型***/
    private UserViewInfo userViewInfo;

    public UserFragment(Context context) {
        super(context);
    }

    @Override
    protected void onViewCreate() {
        super.onViewCreate();
        userViewInfo = (UserViewInfo) getBeanViewInfo();
        imageView.setOnLongClickListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                Toast.showShort(mContext, "长按事件:" + userViewInfo.getUser());
            }
        });
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_image_photo_layout, null, false);
    }
}
