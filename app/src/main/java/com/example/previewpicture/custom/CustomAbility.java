package com.example.previewpicture.custom;

import com.example.previewpicture.ResourceTable;
import com.previewlibrary.GPreviewAbility;
import com.previewlibrary.utils.Toast;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;

/**
 * CustomAbility.
 *
 * @author author
 * @version version
 */
public class CustomAbility extends GPreviewAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        findComponentById(ResourceTable.Id_testBtn).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Toast.showShort(CustomAbility.this, "测试");
                //退出时调用，d封装方法的 不然没有动画效果
                transformOut();
            }
        });
        findComponentById(ResourceTable.Id_back).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                //退出时调用，d封装方法的 不然没有动画效果
                transformOut();
            }
        });
    }

    @Override
    public int setContentLayout() {
        return ResourceTable.Layout_ability_custom;
    }
}
