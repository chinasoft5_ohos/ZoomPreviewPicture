package com.example.previewpicture;

import com.example.previewpicture.bean.UserViewInfo;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * ImageUrlConfig
 * <p/>
 * Created by woxingxiao on 2016-10-24.
 */
public final class ImageUrlConfig {

    private static List<String> sUrls = new ArrayList<>();
    private static List<UserViewInfo> videos = new ArrayList<>();

    /**
     * getVideos .
     *
     * @param context context
     * @return List
     */
    public static List<UserViewInfo> getVideos(Context context) {
        videos.clear();
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video1), context.getString(ResourceTable.String_video1_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video2),
                context.getString(ResourceTable.String_video2_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video3_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video4),
                context.getString(ResourceTable.String_video4_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video5),
                context.getString(ResourceTable.String_video5_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video6_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video7),
                context.getString(ResourceTable.String_video7_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video8_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video9),
                context.getString(ResourceTable.String_video9_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video10_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video11),
                context.getString(ResourceTable.String_video11_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video12_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video13),
                context.getString(ResourceTable.String_video13_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video14_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video15),
                context.getString(ResourceTable.String_video15_p)));
        videos.add(new UserViewInfo(context.getString(ResourceTable.String_video16_p)));
        return videos;
    }

    /**
     * getUrls .
     *
     * @param context context
     * @return List
     */
    public static List<String> getUrls(Context context) {
        sUrls.clear();
        String[] stringArray = context.getStringArray(ResourceTable.Strarray_picimage);
        for (int i = 0; i < stringArray.length; i++) {
            sUrls.add(stringArray[i]);
        }
        return sUrls;
    }

    /**
     * getGifUrls .
     *
     * @param context context
     * @return List
     */
    public static List<UserViewInfo> getGifUrls(Context context) {
        String[] stringArray = context.getStringArray(ResourceTable.Strarray_gifimage);
        List<UserViewInfo> userViewInfos = new ArrayList<>();
        for (int i = 0; i < stringArray.length; i++) {
            userViewInfos.add(new UserViewInfo(stringArray[i]));
        }
        return userViewInfos;
    }
}
