package com.example.previewpicture.nine.ability;

import com.example.previewpicture.ImageUrlConfig;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.bean.UserViewInfo;
import com.example.previewpicture.nine.adapter.PostAdapter;
import com.example.previewpicture.nine.entity.Post;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ListContainer;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

/**
 * GridStyleAbility.
 *
 * @author author
 * @version version
 */
public class GridStyleAbility extends Ability {
    private ListContainer mRvPostLister;
    private PostAdapter mNineImageAdapter;
    private List<Post> mPostList;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_grid_style);
        int color = getColor(ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(color);
        mRvPostLister = (ListContainer) findComponentById(ResourceTable.Id_rv_post_list);
        mPostList = new ArrayList<>();
        for (int i = 0; i < 29; i++) {
            List<UserViewInfo> imgUrls = new ArrayList<>();
            UserViewInfo userViewInfo;
            SecureRandom ss = new SecureRandom();
            for (int j = 0; j < ss.nextInt(9); j++) {
                userViewInfo = new UserViewInfo(ImageUrlConfig.getUrls(this).get(j));
                imgUrls.add(userViewInfo);
            }
            Post post = new Post("Am I handsome? Am I handsome? Am I handsome?", imgUrls);
            mPostList.add(post);
        }
        mNineImageAdapter = new PostAdapter(this, mPostList);
        mRvPostLister.setItemProvider(mNineImageAdapter);
        mRvPostLister.scrollToCenter(5);
    }
}
