package com.example.previewpicture;

import com.example.previewpicture.custom.GridViewCustomAbility;
import com.example.previewpicture.list.GridView2Ability;
import com.example.previewpicture.list.ListView2Ability;
import com.example.previewpicture.nine.ability.GridStyleAbility;
import com.example.previewpicture.pager.ViewPagerAbility;
import com.example.previewpicture.rec.RecycleViewAbility;
import com.example.previewpicture.video.VideoViewAbility;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

/**
 * MainAbility.
 *
 * @author author
 * @version version
 */
public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        int color = getColor(ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(color);
        initView();
    }

    private void initView() {

        findComponentById(ResourceTable.Id_button).setClickedListener(component -> startExampleAbility(ListView2Ability.class.getCanonicalName()));
        findComponentById(ResourceTable.Id_button2).setClickedListener(component -> startExampleAbility(RecycleViewAbility.class.getCanonicalName()));
        findComponentById(ResourceTable.Id_button3).setClickedListener(component -> startExampleAbility(GridStyleAbility.class.getCanonicalName()));
        findComponentById(ResourceTable.Id_button4).setClickedListener(component -> startExampleAbility(GridView2Ability.class.getCanonicalName()));
        findComponentById(ResourceTable.Id_button6).setClickedListener(component -> startExampleAbility(GridViewCustomAbility.class.getCanonicalName(), 0));
        findComponentById(ResourceTable.Id_button7).setClickedListener(component -> startExampleAbility(GridViewCustomAbility.class.getCanonicalName(), 1));
        findComponentById(ResourceTable.Id_button8).setClickedListener(component -> startExampleAbility(GridViewCustomAbility.class.getCanonicalName(), 2));
        findComponentById(ResourceTable.Id_button10).setClickedListener(component -> startExampleAbility(ViewPagerAbility.class.getCanonicalName()));
        findComponentById(ResourceTable.Id_button11).setClickedListener(component -> startExampleAbility(VideoViewAbility.class.getCanonicalName()));

    }


    private void startExampleAbility(String withAbilityName) {
        Intent intent = new Intent();
        Operation opt = new Intent
                .OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(withAbilityName)
                .build();
        intent.setOperation(opt);
        startAbility(intent);
    }

    private void startExampleAbility(String withAbilityName, int type) {
        Intent intent = new Intent();
        Operation opt = new Intent
                .OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(withAbilityName)
                .build();
        intent.setOperation(opt);
        intent.setParam("type", type);
        startAbility(intent);
    }
}
