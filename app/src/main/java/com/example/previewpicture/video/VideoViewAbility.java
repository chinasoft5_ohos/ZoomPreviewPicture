package com.example.previewpicture.video;

import com.example.previewpicture.ImageUrlConfig;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.bean.UserViewInfo;
import com.example.previewpicture.custom.UserFragment;
import com.example.previewpicture.rec.EasyGridProvider;
import com.previewlibrary.GPreviewBuilder;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Rect;

import java.util.ArrayList;
import java.util.List;

/**
 * VideoViewAbility.
 *
 * @author author
 * @version version
 */
public class VideoViewAbility extends Ability {
    private ArrayList<UserViewInfo> mThumbViewInfoList = new ArrayList<>();
    private ListContainer mRecyclerView;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_video_view);
        int color = getColor(ResourceTable.Color_colorPrimaryDark);
        getWindow().setStatusBarColor(color);
        initView();
    }

    private void initView() {
        //准备数据
        List<String> urls = ImageUrlConfig.getUrls(this);
        mThumbViewInfoList.addAll(ImageUrlConfig.getVideos(this));
        for (int i = 0; i < 12; i++) {
            mThumbViewInfoList.add(new UserViewInfo(urls.get(i)));
        }
        mThumbViewInfoList.add(new UserViewInfo(getString(ResourceTable.String_gif_1)));
        mThumbViewInfoList.add(new UserViewInfo(getString(ResourceTable.String_image_1)));
        mThumbViewInfoList.addAll(ImageUrlConfig.getVideos(this));
        mThumbViewInfoList.addAll(ImageUrlConfig.getGifUrls(this));

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_listView);
        MyBaseQuickAdapter myBaseQuickAdapter = new MyBaseQuickAdapter(this);
        myBaseQuickAdapter.setData(mThumbViewInfoList);
        myBaseQuickAdapter.setNumColumns(2);
        myBaseQuickAdapter.setGridSpacing(0);
        mRecyclerView.setItemProvider(myBaseQuickAdapter);
        myBaseQuickAdapter.notifyDataChanged();
        myBaseQuickAdapter.setOnItemClickListener(new EasyGridProvider.OnItemClickListener() {
            @Override
            public void onItemClick(Component component, int position) {
                computeBoundsBackward(position);
                GPreviewBuilder.from(VideoViewAbility.this)
                        .setData(mThumbViewInfoList)
                        .setUserFragment(UserFragment.class)
                        .setCurrentIndex(position)
                        .setSingleFling(true)
                        .setType(GPreviewBuilder.IndicatorType.Number)
                        .start();
            }
        });
    }

    private void computeBoundsBackward(int postion) {
        for (int i = 0; i < mThumbViewInfoList.size(); i++) {
            ComponentContainer itemView = (ComponentContainer) mRecyclerView.getComponentAt(i / 2);
            Rect bounds = new Rect();
            if (itemView != null) {
                Component component = itemView.getComponentAt(i % 2 == 0 ? 0 : 1);
                if (component != null) {
                    Image thumbView = (Image) component.findComponentById(ResourceTable.Id_iv);
                    thumbView.getSelfVisibleRect(bounds);
                    int width = bounds.getWidth();
                    int height = bounds.getHeight();
                    int[] locationOnScreen = thumbView.getLocationOnScreen();
                    int left = locationOnScreen[0];
                    int top = locationOnScreen[1];
                    bounds.set(left, top, left + width, top + height);
                }
            }
            mThumbViewInfoList.get(i).setBounds(bounds);
        }
    }
}
