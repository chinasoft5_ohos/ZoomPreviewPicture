package com.example.previewpicture.video;

import com.previewlibrary.GPVideoPlayerAbility;
import ohos.aafwk.content.Intent;

/**
 * VideoPlayerDetailedAbility.
 *
 * @author author
 * @version version
 */
public class VideoPlayerDetailedAbility extends GPVideoPlayerAbility {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        // super.setUIContent(ResourceTable.Layout_ability_video_player_detailed);
    }
}
