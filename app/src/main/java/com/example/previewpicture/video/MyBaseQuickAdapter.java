package com.example.previewpicture.video;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.previewpicture.ResourceTable;
import com.example.previewpicture.app.GlideBitmapTransformation;
import com.example.previewpicture.bean.UserViewInfo;
import com.example.previewpicture.rec.EasyGridProvider;

import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import org.jetbrains.annotations.Nullable;

/**
 * MyBaseQuickAdapter.
 *
 * @author author
 * @version version
 */
public class MyBaseQuickAdapter extends EasyGridProvider<UserViewInfo> {

    public MyBaseQuickAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    protected void bind(ViewHolder holder, UserViewInfo item, int position) {
        Image thumbView = holder.getView(ResourceTable.Id_iv);
        Image btnVideo = holder.getView(ResourceTable.Id_btnVideo);
        if (item.getVideoUrl() == null) {
            btnVideo.setVisibility(Component.HIDE);
        } else {
            btnVideo.setVisibility(Component.VISIBLE);
        }
        thumbView.setVisibility(Component.VISIBLE);

        Glide.with(mContext)
                .load(item.getUrl())
                .apply(RequestOptions.bitmapTransform(new GlideBitmapTransformation())
                        .error(ResourceTable.Media_ic_iamge_zhanwei))
                .listener(new RequestListener<Element>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o, Target<Element> target, boolean b) {
                        thumbView.setPixelMap(ResourceTable.Media_ic_iamge_zhanwei);
                        return true;
                    }

                    @Override
                    public boolean onResourceReady(Element element, Object o, Target<Element> target, DataSource dataSource, boolean b) {
                        return false;
                    }
                })
                .into(thumbView);
    }

    @Override
    protected void bindPlaceholder(ViewHolder holder, int position) {
        Image iv = holder.getView(ResourceTable.Id_iv);
        Image btnVideo = holder.getView(ResourceTable.Id_btnVideo);
        iv.setVisibility(Component.HIDE);
        btnVideo.setVisibility(Component.HIDE);
    }

    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_item_image_grid;
    }
}
