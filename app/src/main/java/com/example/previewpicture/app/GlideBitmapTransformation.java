/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.previewpicture.app;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import ohos.media.image.PixelMap;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;

/**
 * GlideBitmapTransformation.
 *
 * @author author
 * @version version
 */
public class GlideBitmapTransformation extends BitmapTransformation {
    /**
     * ID.
     */
    private static final String ID = "com.example.previewpicture.app.GlideBitmapTransformation";
    /**
     * ID_BYTES.
     */
    private static final byte[] ID_BYTES = ID.getBytes(CHARSET);

    @Override
    protected PixelMap transform(@NotNull BitmapPool pool, @NotNull PixelMap toTransform, int outWidth, int outHeight) {

        return TransformationUtils.centerCrop(toTransform, outWidth, outHeight);
    }

    @Override
    public void updateDiskCacheKey(@NotNull MessageDigest messageDigest) {
        messageDigest.update(ID_BYTES);
    }
}
