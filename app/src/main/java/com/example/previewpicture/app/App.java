package com.example.previewpicture.app;

import com.previewlibrary.ZoomMediaLoader;
import ohos.aafwk.ability.AbilityPackage;

/**
 * App.
 *
 * @author author
 * @version version
 */
public class App extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        ZoomMediaLoader.getInstance().init(new TestImageLoader());
    }
}
