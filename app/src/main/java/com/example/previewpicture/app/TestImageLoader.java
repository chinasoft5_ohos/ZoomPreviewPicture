package com.example.previewpicture.app;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.previewpicture.ResourceTable;
import com.previewlibrary.loader.IZoomMediaLoader;
import com.previewlibrary.loader.MySimpleTarget;

import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.app.Context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by yangc on 2017/9/4.
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated:
 */
public class TestImageLoader implements IZoomMediaLoader {
    @Override
    public void displayImage(@NotNull Context context, @NotNull String path, Image imageView, @NotNull MySimpleTarget simpleTarget) {
        Glide.with(context)
                .load(path)
                .error(ResourceTable.Media_ic_default_image)
                .into(new SimpleTarget<Element>() {
                    @Override
                    public void onResourceReady(@NotNull Element element, @Nullable Transition<? super Element> transition) {
                        simpleTarget.onResourceReady();
                        imageView.setImageElement(element);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Element errorDrawable) {
                        simpleTarget.onLoadFailed(errorDrawable);
                        super.onLoadFailed(errorDrawable);
                    }
                });

    }

    @Override
    public void displayGifImage(@NotNull Context context, @NotNull String path, Image imageView, @NotNull MySimpleTarget simpleTarget) {
        Glide.with(context)
                .load(path)
                .error(ResourceTable.Media_ic_default_image)
                .into(new SimpleTarget<Element>() {
                    @Override
                    public void onResourceReady(@NotNull Element element, @Nullable Transition<? super Element> transition) {
                        simpleTarget.onResourceReady();
                        imageView.setImageElement(element);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Element errorDrawable) {
                        simpleTarget.onLoadFailed(errorDrawable);
                        super.onLoadFailed(errorDrawable);
                    }
                });
    }

    @Override
    public void onStop(@NotNull Context context) {

    }

    @Override
    public void clearMemory(@NotNull Context c) {

    }
}
