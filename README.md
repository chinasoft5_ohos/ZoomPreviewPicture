# ZoomPreviewPicture

#### 项目介绍
- 项目名称：ZoomPreviewPicture
- 所属系列：openharmony的第三方组件适配移植
- 功能：本项目受Zooming a View 启发，实现了点击小图放大至全屏预览，退出全屏恢复至原来位置这两个过程的动画过渡。 常见应用场景如微信朋友圈照片九宫格和微信聊天图片,视频,gif预览，某些手机系统相册等viewpager图片查看 缩放 拖拽下拉缩小退出（效果同微信图片浏览）
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：2.3.4

#### 效果演示
![输入图片说明](./image/ZoomPreviewPicture.gif "gif")

#### 安装教程
1.在项目根目录下的build.gradle文件中，

 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}

 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ZoomPreviewPicture:1.0.0')
    ......  
 }

 ```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明
##### 初始化
 实现自定义类，在你 app onInitialize() 中
````java
    @Override
      public void onInitialize() {
          super.onInitialize();
          ZoomMediaLoader.getInstance().init(new TestImageLoader());
      }
````
使用自定义图片加载配置,注意这个必须实现哦。不然不会加载
在你项目工程，创建一个类 实现接口IZoomMediaLoader接口 如下代码，demo 采用glide ，可以使用Picassor Imagloader 图片加载框架
```java
public class TestImageLoader implements IZoomMediaLoader {

    @Override
    public void displayImage(@NotNull Context context, @NotNull String path, Image imageView, @NotNull MySimpleTarget simpleTarget) {
        Glide.with(context)
                .load(path)
                .error(ResourceTable.Media_ic_default_image)
                .into(new SimpleTarget<Element>() {
                    @Override
                    public void onResourceReady(@NotNull Element element, @Nullable Transition<? super Element> transition) {
                        simpleTarget.onResourceReady();
                        imageView.setImageElement(element);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Element errorDrawable) {
                        simpleTarget.onLoadFailed(errorDrawable);
                        super.onLoadFailed(errorDrawable);
                    }
                });

    }

    @Override
    public void displayGifImage(@NotNull Context context, @NotNull String path, Image imageView, @NotNull MySimpleTarget simpleTarget) {

    }

    @Override
    public void onStop(@NotNull Context context) {

    }

    @Override
    public void clearMemory(@NotNull Context c) {

    }
}

```
##### 使用方式
```java
     GPreviewBuilder.from(GridViewCustomAbility.this)//Ability实例必须
                            .to(CustomAbility.class)//自定义Ability 使用默认的预览不需要
                            .setData(mThumbViewInfoList)//集合
                            .setUserFragment(UserFragment.class)//自定义Fragment 使用默认的预览不需要
                            .setCurrentIndex(position)
                            .setSingleFling(false)//是否在黑屏区域点击返回
                            .setDrag(false)//是否禁用图片拖拽返回  
                            .setType(GPreviewBuilder.IndicatorType.Dot)//指示器类型
                            .start();//启动            

```
构造实体类： 你的实体类实现IThumbViewInfo接口 
IThumbViewInfo 实现 Sequenceable 接口 注意序列化化
 ```java
 public class UserViewInfo implements IThumbViewInfo {
     private String url;  //图片地址
     private Rect mBounds; // 记录坐标
     private String user;//
      private String videoUrl;//视频链接 //不为空是视频
      
     public UserViewInfo(String url) {
         this.url = url;
     }
 
     @Override
     public String getUrl() {//将你的图片地址字段返回
         return url;
     }
     public void setUrl(String url) {
         this.url = url;
     }
     @Override
     public Rect getBounds() {//将你的图片显示坐标字段返回
         return mBounds;
     }
     
     public void setBounds(Rect bounds) {
         mBounds = bounds;
     }
    } 
```
##### 列表使用方式
ListContainer为例列表控件item点击事件添加相应代码。
(demo有九宫格控件实例代码)
```java
     listView.setItemClickedListener(new ListContainer.ItemClickedListener() {
                @Override
                public void onItemClicked(ListContainer listContainer, Component component, int position, long l) {
                    computeBoundsBackward(position);
                    GPreviewBuilder.from(ListView2Ability.this)
                            .setData(mThumbViewInfoList)
                            .setCurrentIndex(position)
                            .isDisableDrag(true, 0.6f)
                            .setType(GPreviewBuilder.IndicatorType.Number)
                            .start();
                }
            });
    /**
     ** 查找信息
     * 从第一个完整可见item逆序遍历，如果初始位置为0，则不执行方法内循环
     */
     private void computeBoundsBackward(int postion) {
            for (int i = 0; i < mThumbViewInfoList.size(); i++) {
                Component itemView = listView.getComponentAt(i);
                Rect bounds = new Rect();
                if (itemView != null) {
                    Image thumbView = (Image) itemView.findComponentById(ResourceTable.Id_iv);
                    thumbView.getSelfVisibleRect(bounds);
                    int width = bounds.getWidth();
                    int height = bounds.getHeight();
                    int[] locationOnScreen = thumbView.getLocationOnScreen();
                    int left = locationOnScreen[0];
                    int top = locationOnScreen[1];
                    bounds.set(left, top, left + width, top + height);
                }
                mThumbViewInfoList.get(i).setBounds(bounds);
            }
        }
```

##### 视频的支持
自定义播放视频控制
```java
   GPreviewBuilder.from(VideoViewAbility.this)
                       .setData(mThumbViewInfoList)
                       .setCurrentIndex(position)
                       .setSingleFling(true)
                      .setOnVideoPlayerListener(new VideoClickListener(){
                           @Override
                           public void onPlayerVideo(String url) {
                              //跳转视屏
                           }
                       })
                       .setType(GPreviewBuilder.IndicatorType.Number)
                       .start();
```

##### 自定义Ability,Fragment

###### 实现自定义Ability，实现你业务操作例如加入标题栏等等
在你的布局中,引用类库核心布局
```xml
<include layout="$layout:ability_gpreview" />
```
###### 实现自定义Fragment   实现自定义业务  例如 长按保存图片，编辑图片,对图片说明内容等等
需要布局自定义重写onCreateView()。引用你自定义布局中添加
```xml
<include layout="$layout:fragment_image_photo_layout" />
```
使用细节注意：
  1. **Ability和Fragment可以单独使用,也可以组合一起使用**
  2. **自定义使用布局时，不在子类使用setUIContent()方法**
  3. **你在Ability 重写 setContentLayout()，返回你的自定义布局**
  4. **在你布局内容 使用include layout="$layout:ability_gpreview" 预览布局添加你布局中**
  5. **GPreviewBuilder 调用 from()方法后，调用to();指向你.to(CustomAbility.class)自定义预览Ability**
  6. **别忘了在config.json  Ability 使用主题**
  示例：
```json
{
                      "orientation": "unspecified",
                      "name": "com.example.previewpicture.custom.CustomAbility",
                      "icon": "$media:icon",
                      "description": "$string:customability_description",
                      "type": "page",
                      "launchType": "standard",
                      "metaData": {
                        "customizeData": [
                          {
                            "name": "hwc-theme",
                            "value": "androidhwext:style/Theme.Emui.Translucent.NoTitleBar"
                          }
                        ]
                      }
                    }
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

- 0.0.1-SNAPSHOT

#### 版权和许可信息

- Apache2.0
