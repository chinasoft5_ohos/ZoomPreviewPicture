/*
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.senab2.photoview2.gestures;

import ohos.app.Context;


/**
 * VersionedGestureDetector.
 *
 * @author author
 * @version version
 */
public final class VersionedGestureDetector {
    /**
     * newInstance .
     *
     * @param context  context
     * @param listener listener
     * @return GestureDetector
     */
    public static GestureDetector newInstance(Context context, OnGestureListener listener) {
        GestureDetector detector = new FroyoGestureDetector(context);
        detector.setOnGestureListener(listener);
        return detector;
    }

}