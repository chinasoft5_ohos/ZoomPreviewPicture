/*
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.senab2.photoview2.gestures;

import ohos.multimodalinput.event.TouchEvent;

/**
 * GestureDetector.
 *
 * @author author
 * @version version
 */
public interface GestureDetector {
    /**
     * onTouchEvent
     *
     * @param ev ev
     * @return boolean
     */
    boolean onTouchEvent(TouchEvent ev);

    /**
     * isScaling.
     *
     * @return boolean
     */
    boolean isScaling();

    /**
     * isDragging.
     *
     * @return boolean
     */
    boolean isDragging();

    /**
     * setOnGestureListener .
     *
     * @param listener listener
     */
    void setOnGestureListener(OnGestureListener listener);

}
