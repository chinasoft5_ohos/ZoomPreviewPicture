/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uk.co.senab2.photoview2;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.media.image.PixelMap;
import ohos.media.image.common.AlphaType;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

/**
 * Compat.
 *
 * @author author
 * @version version
 */
public class Compat {
    /**
     * elementToPixelMap
     *
     * @param element element
     * @return PixelMap
     */
    public static PixelMap elementToPixelMap(Element element) {
        if (element instanceof PixelMapElement) {
            return ((PixelMapElement) element).getPixelMap();
        }
        int ww = element.getWidth();
        int hh = element.getHeight();

        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(ww, hh);
        options.pixelFormat = PixelFormat.ARGB_8888;
        options.alphaType = AlphaType.UNPREMUL;
        options.editable = true;

        PixelMap pixelMap = PixelMap.create(options);

        Canvas canvas = new Canvas(new Texture(pixelMap));
        element.setBounds(0, 0, ww, hh);
        element.drawToCanvas(canvas);

        return pixelMap;
    }
}
