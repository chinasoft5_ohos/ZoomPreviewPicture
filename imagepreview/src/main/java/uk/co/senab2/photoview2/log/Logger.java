/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uk.co.senab2.photoview2.log;

/**
 * interface for a logger class to replace the static calls to.
 *
 * @author author
 * @version version
 */
public interface Logger {
    /**
     * v
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    int v(String tag, String msg);

    /**
     * v .
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    int v(String tag, String msg, Throwable tr);

    /**
     * d
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    int d(String tag, String msg);

    /**
     * d .
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    int d(String tag, String msg, Throwable tr);

    /**
     * i
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    int i(String tag, String msg);

    /**
     * i .
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    int i(String tag, String msg, Throwable tr);

    /**
     * w
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    int w(String tag, String msg);

    /**
     * w.
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    int w(String tag, String msg, Throwable tr);

    /**
     * e
     *
     * @param tag tag
     * @param msg msg
     * @return int
     */
    int e(String tag, String msg);

    /**
     * e .
     *
     * @param tag tag
     * @param msg msg
     * @param tr  tr
     * @return int
     */
    int e(String tag, String msg, Throwable tr);
}
