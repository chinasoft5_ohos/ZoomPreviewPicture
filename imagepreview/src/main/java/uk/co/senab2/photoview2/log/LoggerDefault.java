/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uk.co.senab2.photoview2.log;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Helper class to redirect {@link LogManager} to {@link ohos.hiviewdfx.HiLog}
 *
 * @author author
 * @version version
 */
public class LoggerDefault implements Logger {

    private static final String TAG_LOG = "LogUtil";

    private static final HiLogLabel LABEL_LOG = new HiLogLabel(0, 0, LoggerDefault.TAG_LOG);

    private static final String LOG_FORMAT = "%{public}s: %{public}s";
    private static final String LOG_FORMAT2 = "%{public}s: %{public}s: %{public}s";

    @Override
    public int v(String tag, String msg) {
        return HiLog.info(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    @Override
    public int v(String tag, String msg, Throwable tr) {
        return HiLog.info(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    @Override
    public int d(String tag, String msg) {
        logger(tag, msg);
        return HiLog.debug(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    @Override
    public int d(String tag, String msg, Throwable tr) {
        return HiLog.debug(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    @Override
    public int i(String tag, String msg) {
        logger(tag, msg);
        return HiLog.info(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    @Override
    public int i(String tag, String msg, Throwable tr) {
        return HiLog.info(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    @Override
    public int w(String tag, String msg) {
        return HiLog.warn(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    @Override
    public int w(String tag, String msg, Throwable tr) {
        return HiLog.warn(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    @Override
    public int e(String tag, String msg) {
        return HiLog.error(LABEL_LOG, LOG_FORMAT, tag, msg);
    }

    @Override
    public int e(String tag, String msg, Throwable tr) {
        return HiLog.error(LABEL_LOG, LOG_FORMAT2, tag, msg, HiLog.getStackTrace(tr));
    }

    private void logger(String tag, String msg) {
        java.util.logging.Logger.getLogger(tag).info(msg);
    }

}


