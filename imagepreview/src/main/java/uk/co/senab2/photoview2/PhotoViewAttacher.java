/*******************************************************************************
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package uk.co.senab2.photoview2;

import java.lang.ref.WeakReference;

import com.previewlibrary.utils.AccelerateDecelerateInterpolator;
import com.previewlibrary.utils.Interpolator;
import com.ryan.ohos.extension.gesture.GestureDetector;

import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Matrix;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.TouchEvent;

import org.jetbrains.annotations.Nullable;

import uk.co.senab2.photoview2.gestures.OnGestureListener;
import uk.co.senab2.photoview2.gestures.VersionedGestureDetector;
import uk.co.senab2.photoview2.log.LogManager;
import uk.co.senab2.photoview2.scrollerproxy.ScrollerProxy;

/**
 * PhotoViewAttacher.
 *
 * @author author
 * @version version
 */
public class PhotoViewAttacher implements IPhotoView,
        Component.TouchEventListener,
        OnGestureListener,
        Component.LayoutRefreshedListener {

    private static final String LOG_TAG = "PhotoViewAttacher";

    // let debug flag be dynamic, but still Proguard can be used to remove from
    // release builds
    private static final boolean DEBUG = true;

    private Interpolator mInterpolator = new AccelerateDecelerateInterpolator();
    int ZOOM_DURATION = DEFAULT_ZOOM_DURATION;

    static final int EDGE_NONE = -1;
    static final int EDGE_LEFT = 0;
    static final int EDGE_RIGHT = 1;
    static final int EDGE_BOTH = 2;

    static int SINGLE_TOUCH = 1;

    private float mMinScale = DEFAULT_MIN_SCALE;
    private float mMidScale = DEFAULT_MID_SCALE;
    private float mMaxScale = DEFAULT_MAX_SCALE;

    private boolean mAllowParentInterceptOnEdge = true;
    private boolean mBlockParentIntercept = false;

    private final EventHandler mHandler = new EventHandler(EventRunner.getMainEventRunner());

    private PageSlider pageSlider;

    private static void checkZoomLevels(float minZoom, float midZoom,
                                        float maxZoom) {
        if (minZoom >= midZoom) {
            throw new IllegalArgumentException(
                    "Minimum zoom has to be less than Medium zoom. Call setMinimumZoom() with a more appropriate value");
        } else if (midZoom >= maxZoom) {
            throw new IllegalArgumentException(
                    "Medium zoom has to be less than Maximum zoom. Call setMaximumZoom() with a more appropriate value");
        }
    }

    /**
     * hasDrawable .
     *
     * @param imageView imageView
     * @return true if the ImageView exists, and its Drawable exists
     */
    private static boolean hasDrawable(Image imageView) {
        return null != imageView && null != imageView.getImageElement();
    }

    /**
     * isSupportedScaleType.
     *
     * @param scaleType scaleType
     * @return true if the ScaleType is supported.
     */
    private static boolean isSupportedScaleType(final Image.ScaleMode scaleType) {
        return scaleType != null;
    }

    /**
     * Sets the ImageView's ScaleType to Matrix.
     *
     * @param imageView imageView
     */
    private static void setImageViewScaleTypeMatrix(Image imageView) {

    }

    private WeakReference<PhotoView> mImageView;

    // Gesture Detectors
    private GestureDetector mGestureDetector;
    private uk.co.senab2.photoview2.gestures.GestureDetector mScaleDragDetector;

    // These are set so we don't keep allocating them on the heap
    private final Matrix mBaseMatrix = new Matrix();
    private final Matrix mDrawMatrix = new Matrix();
    private final Matrix mSuppMatrix = new Matrix();
    private final RectFloat mDisplayRect = new RectFloat();
    private final float[] mMatrixValues = new float[9];

    // Listeners
    private OnMatrixChangedListener mMatrixChangeListener;
    private OnPhotoTapListener mPhotoTapListener;
    private OnViewTapListener mViewTapListener;
    private Component.LongClickedListener mLongClickListener;
    private OnScaleChangeListener mScaleChangeListener;
    private OnSingleFlingListener mSingleFlingListener;

    private int mIvTop, mIvRight, mIvBottom, mIvLeft;
    private FlingRunnable mCurrentFlingRunnable;
    private int mScrollEdge = EDGE_BOTH;
    private float mBaseRotation;
    private boolean mZoomEnabled;
    private Image.ScaleMode mScaleType = Image.ScaleMode.ZOOM_CENTER;

    public PhotoViewAttacher(PhotoView imageView) {
        this(imageView, true);
    }

    public PhotoViewAttacher(PhotoView imageView, boolean zoomable) {
        mImageView = new WeakReference<>(imageView);

        // imageView.setDrawingCacheEnabled(true);
        imageView.setTouchEventListener(this);
        imageView.setLayoutRefreshedListener(this);

        // Make sure we using MATRIX Scale Type
        setImageViewScaleTypeMatrix(imageView);

        // Create Gesture Detectors...
        mScaleDragDetector = VersionedGestureDetector.newInstance(
                imageView.getContext(), this);

        mGestureDetector = new GestureDetector(imageView.getContext(),
                new GestureDetector.SimpleOnGestureListener() {
                    // forward long click listener
                    @Override
                    public void onLongPress(TouchEvent e) {
                        if (null != getImageView() && null != mLongClickListener && getImageView().getContentPositionY() == 0 && getImageView().getContentPositionX() == 0) {
                            mLongClickListener.onLongClicked(getImageView());
                        }
                    }

                    @Override
                    public boolean onFling(
                            TouchEvent e1, TouchEvent e2,
                            float velocityX, float velocityY) {
                        if (mSingleFlingListener != null) {
                            if (getScaleFloat() > DEFAULT_MIN_SCALE) {
                                return false;
                            }

                            if (e1.getPointerCount() > SINGLE_TOUCH
                                    || e2.getPointerCount() > SINGLE_TOUCH) {
                                return false;
                            }

                            return mSingleFlingListener.onFling(e1, e2, velocityX, velocityY);
                        }
                        return false;
                    }
                });
        mGestureDetector.setOnDoubleTapListener(new DefaultOnDoubleTapListener(this));
        mBaseRotation = 0.0f;
        // Finally, update the UI so that we're zoomable
        setZoomable(zoomable);
    }

    /**
     * setPageSlider .
     *
     * @param pageSlider pageSlider
     */
    public void setPageSlider(PageSlider pageSlider) {
        this.pageSlider = pageSlider;
    }

    /**
     * getPageSlider.
     *
     * @return PageSlider
     */
    public PageSlider getPageSlider() {
        return pageSlider;
    }

    @Override
    public void setOnDoubleTapListener(GestureDetector.OnDoubleTapListener newOnDoubleTapListener) {
        if (newOnDoubleTapListener != null) {
            this.mGestureDetector.setOnDoubleTapListener(newOnDoubleTapListener);
        } else {
            this.mGestureDetector.setOnDoubleTapListener(new DefaultOnDoubleTapListener(this));
        }
    }

    @Override
    public void setOnScaleChangeListener(OnScaleChangeListener onScaleChangeListener) {
        this.mScaleChangeListener = onScaleChangeListener;
    }

    @Override
    public void setOnSingleFlingListener(OnSingleFlingListener onSingleFlingListener) {
        this.mSingleFlingListener = onSingleFlingListener;
    }


    @Override
    public boolean canZoom() {
        return mZoomEnabled;
    }

    /**
     * Clean-up the resources attached to this object. This needs to be called when the ImageView is
     * {@link PhotoView}.
     */
    @SuppressWarnings("deprecation")
    public void cleanup() {
        if (null == mImageView) {
            return; // cleanup already done
        }

        final Image imageView = mImageView.get();

        if (null != imageView) {
            // Remove this as a global layout listener
            imageView.setLayoutRefreshedListener(null);
            // Remove the ImageView's reference to this
            imageView.setTouchEventListener(null);

            // make sure a pending fling runnable won't be run
            cancelFling();
        }

        if (null != mGestureDetector) {
            mGestureDetector.setOnDoubleTapListener(null);
        }

        // Clear listeners too
        mMatrixChangeListener = null;
        mPhotoTapListener = null;
        mViewTapListener = null;

        // Finally, clear ImageView
        mImageView = null;
    }

    @Override
    public RectFloat getDisplayRect() {
        checkMatrixBounds();
        return getDisplayRect(getDrawMatrix());
    }

    @Override
    public boolean setDisplayMatrix(Matrix finalMatrix) {
        if (finalMatrix == null) {
            throw new IllegalArgumentException("Matrix cannot be null");
        }

        Image imageView = getImageView();
        if (null == imageView) {
            return false;
        }

        if (null == imageView.getImageElement()) {
            return false;
        }

        mSuppMatrix.setMatrix(finalMatrix);
        setImageViewMatrix(getDrawMatrix());
        checkMatrixBounds();

        return true;
    }

    /**
     * setBaseRotation .
     *
     * @param degrees degrees
     */
    public void setBaseRotation(final float degrees) {
        mBaseRotation = degrees % 360;
        update();
        setRotationBy(mBaseRotation);
        checkAndDisplayMatrix();
    }

    @Override
    public void setRotationTo(float degrees) {
        mSuppMatrix.setRotate(degrees % 360);
        checkAndDisplayMatrix();
    }

    @Override
    public void setRotationBy(float degrees) {
        mSuppMatrix.postRotate(degrees % 360);
        checkAndDisplayMatrix();
    }

    /**
     * getImageView .
     *
     * @return PhotoView
     */
    public PhotoView getImageView() {
        PhotoView imageView = null;
        if (null != mImageView) {
            imageView = mImageView.get();
        }
        // If we don't have an ImageView, call cleanup()
        if (null == imageView) {
            cleanup();
            LogManager.getLogger().i(LOG_TAG,
                    "ImageView no longer exists. You should not use this PhotoViewAttacher any more.");
        }
        return imageView;
    }

    @Override
    public float getMinimumScale() {
        return mMinScale;
    }

    @Override
    public float getMediumScale() {
        return mMidScale;
    }

    @Override
    public float getMaximumScale() {
        return mMaxScale;
    }


    @Override
    public float getScaleFloat() {
        return (float) Math.sqrt((float) Math.pow(getValue(mSuppMatrix, 0), 2) + (float) Math.pow(getValue(mSuppMatrix, 3), 2));
    }

    @Override
    public Image.ScaleMode getScaleType() {
        return mScaleType;
    }

    @Override
    public void onDrag(float dx, float dy) {
        if (mScaleDragDetector.isScaling()) {
            return; // Do not drag if we are already scaling
        }

        if (DEBUG) {
            LogManager.getLogger().d(LOG_TAG,
                    String.format("onDrag: dx: %.2f. dy: %.2f", dx, dy));
        }

        mSuppMatrix.postTranslate(dx, dy);
        checkAndDisplayMatrix();

        /**
         * Here we decide whether to let the ImageView's parent to start taking
         * over the touch event.
         *
         * First we check whether this function is enabled. We never want the
         * parent to take over if we're scaling. We then check the edge we're
         * on, and the direction of the scroll (i.e. if we're pulling against
         * the edge, aka 'overscrolling', let the parent take over).
         */
        if (mAllowParentInterceptOnEdge && !mScaleDragDetector.isScaling() && !mBlockParentIntercept) {
            if (mScrollEdge == EDGE_BOTH
                    || (mScrollEdge == EDGE_LEFT && dx >= 1f)
                    || (mScrollEdge == EDGE_RIGHT && dx <= -1f)) {
                if (pageSlider != null) {
                    pageSlider.setEnabled(true);
                }
            }
        } else {
            if (pageSlider != null) {
                pageSlider.setEnabled(false);
            }
        }
    }

    @Override
    public void onFling(
            float startX, float startY,
            float velocityX, float velocityY) {
        if (DEBUG) {
            LogManager.getLogger().d(
                    LOG_TAG,
                    "onFling. sX: " + startX + " sY: " + startY + " Vx: "
                            + velocityX + " Vy: " + velocityY);
        }
        Image imageView = getImageView();
        mCurrentFlingRunnable = new FlingRunnable(imageView.getContext());
        mCurrentFlingRunnable.fling(getImageViewWidth(imageView),
                getImageViewHeight(imageView), (int) velocityX, (int) velocityY);
        mHandler.postTask(mCurrentFlingRunnable);
    }


    @Override
    public void onRefreshed(Component component) {
        Image imageView = getImageView();

        if (null != imageView) {
            if (mZoomEnabled) {
                final int top = imageView.getTop();
                final int right = imageView.getRight();
                final int bottom = imageView.getBottom();
                final int left = imageView.getLeft();

                /**
                 * We need to check whether the ImageView's bounds have changed.
                 * This would be easier if we targeted API 11+ as we could just use
                 * View.OnLayoutChangeListener. Instead we have to replicate the
                 * work, keeping track of the ImageView's bounds and then checking
                 * if the values change.
                 */
                if (top != mIvTop || bottom != mIvBottom || left != mIvLeft
                        || right != mIvRight) {
                    // Update our base matrix, as the bounds have changed
                    updateBaseMatrix(imageView.getImageElement());

                    // Update values as something has changed
                    mIvTop = top;
                    mIvRight = right;
                    mIvBottom = bottom;
                    mIvLeft = left;
                }
            } else {
                updateBaseMatrix(imageView.getImageElement());
            }
        }
    }

    @Override
    public void onScale(float scaleFactor, float focusX, float focusY) {
        if (DEBUG) {
            LogManager.getLogger().d(
                    LOG_TAG,
                    String.format("onScale: scale: %.2f. fX: %.2f. fY: %.2f",
                            scaleFactor, focusX, focusY));
        }

        if ((getScaleFloat() < mMaxScale || scaleFactor < 1f) && (getScaleFloat() > mMinScale || scaleFactor > 1f)) {
            if (null != mScaleChangeListener) {
                mScaleChangeListener.onScaleChange(scaleFactor, focusX, focusY);
            }
            mSuppMatrix.postScale(scaleFactor, scaleFactor, focusX, focusY);
            checkAndDisplayMatrix();
        }
    }

    @Override
    public boolean dispatchTouchEvent(TouchEvent event) {
        PhotoView imageView = getImageView();
        if (imageView != null) {
            return imageView.dispatchTouchEvent(event);
        }
        return false;
    }

    private boolean onTouch(TouchEvent ev) {
        boolean handled = false;
        switch (ev.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (pageSlider != null) {
                    pageSlider.setEnabled(false);
                }
                cancelFling();
                break;
            case TouchEvent.CANCEL:
            case TouchEvent.PRIMARY_POINT_UP:
                if (getScaleFloat() < mMinScale) {
                    RectFloat rect = getDisplayRect();
                    if (null != rect) {
                        Point center = rect.getCenter();
                        mHandler.postTask(new AnimatedZoomRunnable(getScaleFloat(), mMinScale,
                                center.getPointX(), center.getPointY()));
                        handled = true;
                    }
                } else if (getScaleFloat() > mMaxScale) {
                    RectFloat rect = getDisplayRect();
                    if (rect != null) {
                        mHandler.postTask(new AnimatedZoomRunnable(getScaleFloat(), mMaxScale,
                                rect.getHorizontalCenter(), rect.getVerticalCenter()));
                        handled = true;
                    }
                }
            default: {
            }
            break;
        }
        if (null != mScaleDragDetector) {
            boolean wasScaling = mScaleDragDetector.isScaling();
            boolean wasDragging = mScaleDragDetector.isDragging();
            handled = mScaleDragDetector.onTouchEvent(ev);
            boolean didntScale = !wasScaling && !mScaleDragDetector.isScaling();
            boolean didntDrag = !wasDragging && !mScaleDragDetector.isDragging();
            mBlockParentIntercept = didntScale && didntDrag;
        }
        if (null != mGestureDetector && mGestureDetector.onTouchEvent(ev)) {
            handled = true;
        }
        return handled;
    }

    @Override
    public boolean onTouchEvent(Component v, TouchEvent ev) {
        if (!mZoomEnabled || !hasDrawable((Image) v)) {
            return false;
        }
        boolean dispatchTouchEvent = dispatchTouchEvent(ev);
        if (dispatchTouchEvent) {
            return true;
        }
        return onTouch(ev);
    }

    @Override
    public void setAllowParentInterceptOnEdge(boolean allow) {
        mAllowParentInterceptOnEdge = allow;
    }

    @Override
    public void setMinimumScale(float minimumScale) {
        checkZoomLevels(minimumScale, mMidScale, mMaxScale);
        mMinScale = minimumScale;
    }

    @Override
    public void setMediumScale(float mediumScale) {
        checkZoomLevels(mMinScale, mediumScale, mMaxScale);
        mMidScale = mediumScale;
    }

    @Override
    public void setMaximumScale(float maximumScale) {
        checkZoomLevels(mMinScale, mMidScale, maximumScale);
        mMaxScale = maximumScale;
    }

    @Override
    public void setScaleLevels(float minimumScale, float mediumScale, float maximumScale) {
        checkZoomLevels(minimumScale, mediumScale, maximumScale);
        mMinScale = minimumScale;
        mMidScale = mediumScale;
        mMaxScale = maximumScale;
    }

    @Override
    public void setOnLongClickListener(Component.LongClickedListener listener) {
        mLongClickListener = listener;
    }

    @Override
    public void setOnMatrixChangeListener(OnMatrixChangedListener listener) {
        mMatrixChangeListener = listener;
    }

    @Override
    public void setOnPhotoTapListener(OnPhotoTapListener listener) {
        mPhotoTapListener = listener;
    }

    @Nullable
    OnPhotoTapListener getOnPhotoTapListener() {
        return mPhotoTapListener;
    }

    @Override
    public void setOnViewTapListener(OnViewTapListener listener) {
        mViewTapListener = listener;
    }

    @Nullable
    OnViewTapListener getOnViewTapListener() {
        return mViewTapListener;
    }

    @Override
    public void setScale(float scale) {
        setScale(scale, false);
    }

    @Override
    public void setScale(float scale, boolean animate) {
        Image imageView = getImageView();

        if (null != imageView) {
            setScale(scale,
                    (imageView.getRight()) / 2,
                    (imageView.getBottom()) / 2,
                    animate);
        }
    }

    @Override
    public void setScale(
            float scale, float focalX,
            float focalY, boolean animate) {
        Image imageView = getImageView();

        if (null != imageView) {
            // Check to see if the scale is within bounds
            if (scale < mMinScale || scale > mMaxScale) {
                LogManager
                        .getLogger()
                        .i(LOG_TAG,
                                "Scale must be within the range of minScale and maxScale");
                return;
            }

            if (animate) {
                mHandler.postTask(new AnimatedZoomRunnable(getScaleFloat(), scale,
                        focalX, focalY));
            } else {
                mSuppMatrix.setScale(scale, scale, focalX, focalY);
                checkAndDisplayMatrix();
            }
        }
    }

    /**
     * Set the zoom interpolator
     *
     * @param interpolator the zoom interpolator
     */
    public void setZoomInterpolator(Interpolator interpolator) {
        mInterpolator = interpolator;
    }

    @Override
    public void setScaleType(Image.ScaleMode scaleType) {
        if (isSupportedScaleType(scaleType) && scaleType != mScaleType) {
            mScaleType = scaleType;

            // Finally update
            update();
        }
    }

    @Override
    public void setZoomable(boolean zoomable) {
        mZoomEnabled = zoomable;
        update();
    }

    /**
     * update.
     */
    public void update() {
        Image imageView = getImageView();

        if (null != imageView) {
            if (mZoomEnabled) {
                // Make sure we using MATRIX Scale Type
                setImageViewScaleTypeMatrix(imageView);

                // Update the base matrix using the current drawable
                updateBaseMatrix(imageView.getImageElement());
            } else {
                // Reset the Matrix...
                resetMatrix();
            }
        }
    }

    /**
     * Get the display matrix.
     *
     * @param matrix target matrix to copy to
     */
    @Override
    public void getDisplayMatrix(Matrix matrix) {
        matrix.setMatrix(getDrawMatrix());
    }

    /**
     * Get the current support matrix.
     *
     * @param matrix matrix
     */
    public void getSuppMatrix(Matrix matrix) {
        matrix.setMatrix(mSuppMatrix);
    }

    private Matrix getDrawMatrix() {
        mDrawMatrix.setMatrix(mBaseMatrix);
        mDrawMatrix.postConcat(mSuppMatrix);
        return mDrawMatrix;
    }

    private void cancelFling() {
        if (null != mCurrentFlingRunnable) {
            mCurrentFlingRunnable.cancelFling();
            mCurrentFlingRunnable = null;
        }
    }

    public Matrix getImageMatrix() {
        return mDrawMatrix;
    }

    /**
     * Helper method that simply checks the Matrix, and then displays the result
     */
    private void checkAndDisplayMatrix() {
        if (checkMatrixBounds()) {
            setImageViewMatrix(getDrawMatrix());
        }
    }

    private void checkImageViewScaleType() {

    }

    private float deltaY(RectFloat rect) {
        final Image imageView = getImageView();
        float deltaY = 0;
        final int viewHeight = getImageViewHeight(imageView);
        final float height = rect.getHeight();
        if (height <= viewHeight) {
            switch (mScaleType) {
                case ZOOM_START:
                    deltaY = -rect.top;
                    break;
                case ZOOM_END:
                    deltaY = viewHeight - height - rect.top;
                    break;
                default:
                    deltaY = (viewHeight - height) / 2 - rect.top;
                    break;
            }
        } else if (rect.top > 0) {
            deltaY = -rect.top;
        } else if (rect.bottom < viewHeight) {
            deltaY = viewHeight - rect.bottom;
        }
        return deltaY;
    }

    private float deltaX(RectFloat rect) {
        final Image imageView = getImageView();
        float deltaX = 0;
        final float width = rect.getWidth();
        final int viewWidth = getImageViewWidth(imageView);
        if (width <= viewWidth) {
            switch (mScaleType) {
                case ZOOM_START:
                    deltaX = -rect.left;
                    break;
                case ZOOM_END:
                    deltaX = viewWidth - width - rect.left;
                    break;
                default:
                    deltaX = (viewWidth - width) / 2 - rect.left;
                    break;
            }
            mScrollEdge = EDGE_BOTH;
        } else if (rect.left > 0) {
            mScrollEdge = EDGE_LEFT;
            deltaX = -rect.left;
        } else if (rect.right < viewWidth) {
            deltaX = viewWidth - rect.right;
            mScrollEdge = EDGE_RIGHT;
        } else {
            mScrollEdge = EDGE_NONE;
        }
        return deltaX;
    }

    private boolean checkMatrixBounds() {
        final Image imageView = getImageView();
        if (null == imageView) {
            return false;
        }
        final RectFloat rect = getDisplayRect(getDrawMatrix());
        if (null == rect) {
            return false;
        }
        float deltaY = deltaY(rect);
        float deltaX = deltaX(rect);
        // Finally actually translate the matrix
        mSuppMatrix.postTranslate(deltaX, deltaY);
        return true;
    }

    /**
     * Helper method that maps the supplied Matrix to the current Drawable
     *
     * @param matrix - Matrix to map Drawable against
     * @return RectF - Displayed Rectangle
     */
    private RectFloat getDisplayRect(Matrix matrix) {
        Image imageView = getImageView();

        if (null != imageView) {
            Element d = imageView.getImageElement();
            if (null != d) {
                mDisplayRect.modify(0, 0, d.getWidth(),
                        d.getHeight());
                matrix.mapRect(mDisplayRect);
                return mDisplayRect;
            }
        }
        return null;
    }

    /**
     * getVisibleRectangleBitmap .
     *
     * @return PixelMap
     */
    public PixelMap getVisibleRectangleBitmap() {
        return null;
    }

    @Override
    public void setZoomTransitionDuration(int milliseconds) {
        if (milliseconds < 0) {
            milliseconds = DEFAULT_ZOOM_DURATION;
        }
        this.ZOOM_DURATION = milliseconds;
    }

    @Override
    public IPhotoView getIPhotoViewImplementation() {
        return this;
    }

    /**
     * Helper method that 'unpacks' a Matrix and returns the required value
     *
     * @param matrix     - Matrix to unpack
     * @param whichValue - Which value from Matrix.M* to return
     * @return float - returned value
     */
    private float getValue(Matrix matrix, int whichValue) {
        // matrix.getValues(mMatrixValues);
        float[] data = matrix.getData();
        if (data != null) {
            for (int i = 0; i < data.length; i++) {
                mMatrixValues[i] = data[i];
            }
        }
        return mMatrixValues[whichValue];
    }

    /**
     * Resets the Matrix back to FIT_CENTER, and then displays it.s
     */
    public void resetMatrix() {
        mSuppMatrix.reset();
        setRotationBy(mBaseRotation);
        setImageViewMatrix(getDrawMatrix());
        checkMatrixBounds();
    }

    private void setImageViewMatrix(Matrix matrix) {
        PhotoView imageView = getImageView();
        if (null != imageView) {

            checkImageViewScaleType();
            imageView.setImageMatrix(matrix);
            // Call MatrixChangedListener if needed
            if (null != mMatrixChangeListener) {
                RectFloat displayRect = getDisplayRect(matrix);
                if (null != displayRect) {
                    mMatrixChangeListener.onMatrixChanged(displayRect);
                }
            }
        }
    }

    /**
     * Calculate Matrix for FIT_CENTER
     *
     * @param d - Drawable being displayed
     */
    private void updateBaseMatrix(Element d) {
        Image imageView = getImageView();
        if (null == imageView || null == d) {
            return;
        }
        final double viewWidth = getImageViewWidth(imageView);
        final double viewHeight = getImageViewHeight(imageView);
        final double drawableWidth = d.getWidth();
        final double drawableHeight = d.getHeight();
        mBaseMatrix.reset();
        setBaseMatrix(viewWidth, viewHeight, drawableWidth, drawableHeight);
        resetMatrix();
    }

    private void setBaseMatrix(double viewWidth, double viewHeight, double drawableWidth, double drawableHeight) {
        final double widthScale = viewWidth / drawableWidth;
        final double heightScale = viewHeight / drawableHeight;
        if (mScaleType == Image.ScaleMode.CENTER) {
            mBaseMatrix.postTranslate((float) ((viewWidth - drawableWidth) / 2D),
                    (float) ((viewHeight - drawableHeight) / 2D));
        } else if (mScaleType == Image.ScaleMode.CLIP_CENTER) {
            double scale = Math.max(widthScale, heightScale);
            mBaseMatrix.postScale((float) scale, (float) scale);
            mBaseMatrix.postTranslate((float) ((viewWidth - drawableWidth * scale) / 2D),
                    (float) ((viewHeight - drawableHeight * scale) / 2D));
        } else if (mScaleType == Image.ScaleMode.INSIDE) {
            double scale = Math.min(1.0D, Math.min(widthScale, heightScale));
            mBaseMatrix.postScale((float) scale, (float) scale);
            mBaseMatrix.postTranslate((float) ((viewWidth - drawableWidth * scale) / 2D),
                    (float) ((viewHeight - drawableHeight * scale) / 2D));
        } else {
            RectFloat mTempSrc = new RectFloat(0, 0, (float) drawableWidth, (float) drawableHeight);
            RectFloat mTempDst = new RectFloat(0, 0, (float) viewWidth, (float) viewHeight);
            if ((int) mBaseRotation % 180 != 0) {
                mTempSrc = new RectFloat(0, 0, (float) drawableHeight, (float) drawableWidth);
            }
            switch (mScaleType) {
                case ZOOM_CENTER:
                    mBaseMatrix
                            .setRectToRect(mTempSrc, mTempDst, Matrix.ScaleToFit.CENTER);
                    break;
                case ZOOM_START:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, Matrix.ScaleToFit.START);
                    break;
                case ZOOM_END:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, Matrix.ScaleToFit.END);
                    break;
                case STRETCH:
                    mBaseMatrix.setRectToRect(mTempSrc, mTempDst, Matrix.ScaleToFit.FILL);
                    break;
                default:
                    break;
            }
        }
    }

    private int getImageViewWidth(Image imageView) {
        if (null == imageView) {
            return 0;
        }
        return imageView.getWidth() - imageView.getPaddingLeft() - imageView.getPaddingRight();
    }

    private int getImageViewHeight(Image imageView) {
        if (null == imageView) {
            return 0;
        }
        return imageView.getHeight() - imageView.getPaddingTop() - imageView.getPaddingBottom();
    }

    /**
     * Interface definition for a callback to be invoked when the internal Matrix has changed for
     * this View.
     *
     * @author Chris Banes
     */
    public interface OnMatrixChangedListener {
        /**
         * Callback for when the Matrix displaying the Drawable has changed. This could be because
         * the View's bounds have changed, or the user has zoomed.
         *
         * @param rect - Rectangle displaying the Drawable's new bounds.
         */
        void onMatrixChanged(RectFloat rect);
    }

    /**
     * Interface definition for callback to be invoked when attached ImageView scale changes
     *
     * @author Marek Sebera
     */
    public interface OnScaleChangeListener {
        /**
         * Callback for when the scale changes
         *
         * @param scaleFactor the scale factor (less than 1 for zoom out, greater than 1 for zoom in)
         * @param focusX      focal point X position
         * @param focusY      focal point Y position
         */
        void onScaleChange(float scaleFactor, float focusX, float focusY);
    }

    /**
     * Interface definition for a callback to be invoked when the Photo is tapped with a single
     * tap.
     *
     * @author Chris Banes
     */
    public interface OnPhotoTapListener {

        /**
         * A callback to receive where the user taps on a photo. You will only receive a callback if
         * the user taps on the actual photo, tapping on 'whitespace' will be ignored.
         *
         * @param view - View the user tapped.
         * @param x    - where the user tapped from the of the Drawable, as percentage of the
         *             Drawable width.
         * @param y    - where the user tapped from the top of the Drawable, as percentage of the
         *             Drawable height.
         */
        void onPhotoTap(Component view, float x, float y);

        /**
         * A simple callback where out of photo happened;
         */
        void onOutsidePhotoTap();
    }

    /**
     * Interface definition for a callback to be invoked when the ImageView is tapped with a single
     * tap.
     *
     * @author Chris Banes
     */
    public interface OnViewTapListener {

        /**
         * A callback to receive where the user taps on a ImageView. You will receive a callback if
         * the user taps anywhere on the view, tapping on 'whitespace' will not be ignored.
         *
         * @param view - View the user tapped.
         * @param x    - where the user tapped from the left of the View.
         * @param y    - where the user tapped from the top of the View.
         */
        void onViewTap(Component view, float x, float y);
    }

    /**
     * Interface definition for a callback to be invoked when the ImageView is fling with a single
     * touch
     *
     * @author tonyjs
     */
    public interface OnSingleFlingListener {

        /**
         * A callback to receive where the user flings on a ImageView. You will receive a callback if
         * the user flings anywhere on the view.
         *
         * @param e1        - MotionEvent the user first touch.
         * @param e2        - MotionEvent the user last touch.
         * @param velocityX - distance of user's horizontal fling.
         * @param velocityY - distance of user's vertical fling.
         * @return boolean b
         */
        boolean onFling(TouchEvent e1, TouchEvent e2, float velocityX, float velocityY);
    }

    private class AnimatedZoomRunnable implements Runnable {

        private final float mFocalX, mFocalY;
        private final long mStartTime;
        private final float mZoomStart, mZoomEnd;

        public AnimatedZoomRunnable(final float currentZoom, final float targetZoom,
                                    final float focalX, final float focalY) {
            mFocalX = focalX;
            mFocalY = focalY;
            mStartTime = System.currentTimeMillis();
            mZoomStart = currentZoom;
            mZoomEnd = targetZoom;
        }

        @Override
        public void run() {
            Image imageView = getImageView();
            if (imageView == null) {
                return;
            }

            float t = interpolate();
            float scale = mZoomStart + t * (mZoomEnd - mZoomStart);
            float deltaScale = scale / getScaleFloat();

            onScale(deltaScale, mFocalX, mFocalY);

            // We haven't hit our target scale yet, so post ourselves again
            if (t < 1f) {
                mHandler.postTask(this, 10);
            }
        }

        private float interpolate() {
            float t = 1f * (System.currentTimeMillis() - mStartTime) / ZOOM_DURATION;
            t = Math.min(1f, t);
            t = mInterpolator.getInterpolation(t);
            return t;
        }
    }

    private class FlingRunnable implements Runnable {

        private final ScrollerProxy mScroller;
        private int mCurrentX, mCurrentY;

        public FlingRunnable(Context context) {
            mScroller = ScrollerProxy.getScroller(context);
        }

        public void cancelFling() {
            if (DEBUG) {
                LogManager.getLogger().d(LOG_TAG, "Cancel Fling");
            }
            mScroller.forceFinished(true);
        }

        public void fling(
                int viewWidth, int viewHeight,
                int velocityX, int velocityY) {
            final RectFloat rect = getDisplayRect();
            if (null == rect) {
                return;
            }

            final int startX = Math.round(-rect.left);
            final int minX, maxX, minY, maxY;

            if (viewWidth < rect.getWidth()) {
                minX = 0;
                maxX = Math.round(rect.getWidth() - viewWidth);
            } else {
                minX = maxX = startX;
            }

            final int startY = Math.round(-rect.top);
            if (viewHeight < rect.getHeight()) {
                minY = 0;
                maxY = Math.round(rect.getHeight() - viewHeight);
            } else {
                minY = maxY = startY;
            }

            mCurrentX = startX;
            mCurrentY = startY;

            if (DEBUG) {
                LogManager.getLogger().d(
                        LOG_TAG,
                        "fling. StartX:" + startX + " StartY:" + startY
                                + " MaxX:" + maxX + " MaxY:" + maxY);
            }

            // If we actually can move, fling the scroller
            if (startX != maxX || startY != maxY) {
                mScroller.fling(startX, startY, velocityX, velocityY, minX,
                        maxX, minY, maxY, 0, 0);
            }
        }

        @Override
        public void run() {
            if (mScroller.isFinished()) {
                // remaining post that should not be handled
                return;
            }

            Image imageView = getImageView();
            if (null != imageView && mScroller.computeScrollOffset()) {

                final int newX = mScroller.getCurrX();
                final int newY = mScroller.getCurrY();

                if (DEBUG) {
                    LogManager.getLogger().d(
                            LOG_TAG,
                            "fling run(). CurrentX:" + mCurrentX + " CurrentY:"
                                    + mCurrentY + " NewX:" + newX + " NewY:"
                                    + newY);
                }

                mSuppMatrix.postTranslate(mCurrentX - newX, mCurrentY - newY);
                setImageViewMatrix(getDrawMatrix());

                mCurrentX = newX;
                mCurrentY = newY;

                // Post On animation
                mHandler.postTask(this, 10);
            }
        }
    }
}
