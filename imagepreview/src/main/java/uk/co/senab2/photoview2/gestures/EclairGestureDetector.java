/*
 * Copyright 2011, 2012 Chris Banes.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.co.senab2.photoview2.gestures;

import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

/**
 * EclairGestureDetector.
 *
 * @author author
 * @version version
 */
public class EclairGestureDetector extends CupcakeGestureDetector {

    private static final int INVALID_POINTER_ID = -1;
    private int mActivePointerId = INVALID_POINTER_ID;
    private int mActivePointerIndex = 0;

    public EclairGestureDetector(Context context) {
        super(context);
    }

    @Override
    float getActiveX(TouchEvent ev) {

        try {
            MmiPoint pointerPosition = ev.getPointerPosition(mActivePointerIndex);
            return pointerPosition.getX();
        } catch (Exception e) {
            MmiPoint pointerPosition = ev.getPointerPosition(ev.getIndex());
            return pointerPosition.getX();
        }
    }

    @Override
    float getActiveY(TouchEvent ev) {
        try {
            MmiPoint pointerPosition = ev.getPointerPosition(mActivePointerIndex);
            return pointerPosition.getY();
        } catch (Exception e) {
            MmiPoint pointerPosition = ev.getPointerPosition(ev.getIndex());
            return pointerPosition.getY();
        }
    }

    @Override
    public boolean onTouchEvent(TouchEvent ev) {
        final int action = ev.getAction();
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                mActivePointerId = ev.getPointerId(0);
                break;
            case TouchEvent.CANCEL:
            case TouchEvent.OTHER_POINT_UP:
                mActivePointerId = INVALID_POINTER_ID;
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                // Ignore deprecation, ACTION_POINTER_ID_MASK and
                // ACTION_POINTER_ID_SHIFT has same value and are deprecated
                // You can have either deprecation or lint target api warning
                final int pointerIndex = ev.getIndex();
                final int pointerId = ev.getPointerId(pointerIndex);
                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mActivePointerId = ev.getPointerId(newPointerIndex);
                    MmiPoint pointerPosition = ev.getPointerPosition(newPointerIndex);
                    mLastTouchX = pointerPosition.getX();
                    mLastTouchY = pointerPosition.getY();
                }
                break;
        }
        mActivePointerIndex = ev.getIndex();
        try {
            return super.onTouchEvent(ev);
        } catch (IllegalArgumentException e) {
            // Fix for support lib bug, happening when onDestroy is
            return true;
        }
    }
}
