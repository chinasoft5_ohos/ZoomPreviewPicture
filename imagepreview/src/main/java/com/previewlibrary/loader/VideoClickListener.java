package com.previewlibrary.loader;

/**
 * author  yangc.
 * date 2018/6/28
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated:
 */
public interface VideoClickListener {
    /**
     * onPlayerVideo .
     *
     * @param url url
     */
    void onPlayerVideo(String url);
}
