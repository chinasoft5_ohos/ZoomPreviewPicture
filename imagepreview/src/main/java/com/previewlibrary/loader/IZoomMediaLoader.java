package com.previewlibrary.loader;

import ohos.agp.components.Image;
import ohos.app.Context;
import org.jetbrains.annotations.NotNull;

/**
 * @author yangc
 * date 2017/9/4
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated: 加载器接口
 */
public interface IZoomMediaLoader {

    /**
     * displayImage .
     *
     * @param context      容器
     * @param path         图片你的路径
     * @param imageView    imageView
     * @param simpleTarget 图片加载状态回调
     ***/
    void displayImage(@NotNull Context context, @NotNull String path, Image imageView, @NotNull MySimpleTarget simpleTarget);

    /**
     * 加载gif 图.
     *
     * @param context      容器
     * @param path         图片你的路径
     * @param imageView    imageView
     * @param simpleTarget 图片加载状态回调
     ***/
    void displayGifImage(@NotNull Context context, @NotNull String path, Image imageView, @NotNull MySimpleTarget simpleTarget);

    /**
     * 停止.
     *
     * @param context 容器
     **/
    void onStop(@NotNull Context context);

    /**
     * 停止.
     *
     * @param c 容器
     **/
    void clearMemory(@NotNull Context c);
}
