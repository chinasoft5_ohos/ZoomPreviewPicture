/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.previewlibrary.utils;

import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * SystemUtils.
 *
 * @author a
 * @version 1.0.0
 */
public class SystemUtils {
    /**
     * fp2px.
     *
     * @param context context
     * @param fp      fp
     * @return float
     */
    public static float fp2px(Context context, float fp) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float sca = display.getAttributes().scalDensity;
        return (float) (fp * sca + 0.5d * (fp >= 0 ? 1 : -1));
    }

    /**
     * vp2px.
     *
     * @param context context
     * @param vp      vp
     * @return float
     */
    public static float vp2px(Context context, float vp) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float dpi = display.getAttributes().densityPixels;
        return (float) (vp * dpi + 0.5d * (vp >= 0 ? 1 : -1));
    }

    /**
     * px2vp.
     *
     * @param context context
     * @param pxValue pxValue
     * @return int
     */
    public static int px2vp(Context context, float pxValue) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        float dpi = display.getAttributes().densityPixels;
        return (int) (pxValue / dpi + 0.5d);
    }

    /**
     * getDisplayWidthInPx.
     *
     * @param context context
     * @return int
     */
    public static int getDisplayWidthInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getSize(point);
        return (int) point.getPointX();
    }

    /**
     * getDisplayHightInPx.
     *
     * @param context context
     * @return int
     */
    public static int getDisplayHightInPx(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        Point point = new Point();
        display.getSize(point);
        return (int) point.getPointY();
    }
}
