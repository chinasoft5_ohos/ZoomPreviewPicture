/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.previewlibrary.utils;

/**
 * AccelerateDecelerateInterpolator.
 *
 * @author author
 * @version version
 */
public class AccelerateDecelerateInterpolator implements Interpolator {
    @Override
    public float getInterpolation(float input) {
        return (float) ((Math.cos(((double) input + 1) * Math.PI) / 2.0) + 0.5);
    }

    /**
     *getCurrentValue.
     *
     *@param input input
     *@param start start
     *@param end   end
     *@return float
     */
    public float getCurrentValue(double input, double start, double end) {
        double interpolation = getInterpolation((float) input);
        return (float) (start + (end - start) * interpolation);
    }
}
