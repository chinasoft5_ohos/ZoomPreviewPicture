/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.previewlibrary.utils;

import ohos.agp.animation.AnimatorValue;

public class ValueAnimator extends AnimatorValue implements AnimatorValue.ValueUpdateListener {
    /**
     * startValue.
     */
    private Object startValue;
    /**
     * endValue.
     */
    private Object endValue;
    /**
     * currentValue.
     */
    private Object currentValue;
    /**
     * currentValue.
     */
    private AnimatorUpdateListener animatorUpdateListener;

    /**
     * ofFloat.
     *
     * @param startValue startValue
     * @param endValue   endValue
     * @return ValueAnimator
     */
    public static ValueAnimator ofFloat(float startValue, float endValue) {
        ValueAnimator anim = new ValueAnimator();
        anim.setValueUpdateListener(anim);
        anim.startValue = startValue;
        anim.endValue = endValue;
        return anim;
    }

    /**
     * ofInt.
     *
     * @param startValue startValue
     * @param endValue   endValue
     * @return ValueAnimator
     */
    public static ValueAnimator ofInt(int startValue, int endValue) {
        ValueAnimator anim = new ValueAnimator();
        anim.setValueUpdateListener(anim);
        anim.startValue = startValue;
        anim.endValue = endValue;
        return anim;
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float value) {
        if (startValue instanceof Float) {
            float curr = ((float) endValue - (float) startValue) * value + (float) startValue;
            this.currentValue = curr;
        }
        if (startValue instanceof Integer) {
            int curr = (int) (((int) endValue - (int) startValue) * value + (int) startValue);
            this.currentValue = curr;
        }
        if (animatorUpdateListener != null) {
            animatorUpdateListener.onAnimationUpdate(this);
        }
    }

    /**
     * addUpdateListener.
     *
     * @param animatorUpdateListener animatorUpdateListener
     */
    public void addUpdateListener(AnimatorUpdateListener animatorUpdateListener) {
        this.animatorUpdateListener = animatorUpdateListener;
    }

    /**
     * getAnimatedValue.
     *
     * @return Object
     */
    public Object getAnimatedValue() {
        return currentValue;
    }

    /**
     * AnimatorUpdateListener.
     */
    public interface AnimatorUpdateListener {
        /**
         * Notifies the occurrence of another frame of the animation.
         *
         * @param animation The animation which was repeated.
         */
        void onAnimationUpdate(ValueAnimator animation);
    }
}
