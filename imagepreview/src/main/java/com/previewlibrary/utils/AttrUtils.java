/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.previewlibrary.utils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;

/**
 * 自定义属性工具类.
 * <p>
 * 注意： 宽高都为 match_content 且无实际内容时构造方法不会调用
 * 使用方法：
 * xxx extends Component
 * 获取自定义属性：
 * String count = AttrUtils.getStringFromAttr(attrSet,"cus_count","0");
 * <p>
 * 属性定义：
 * 布局头中加入  xmlns:hap="http://schemas.huawei.com/apk/res/ohos" 使用hap区分自定义属性与系统属性
 * 即可使用 hap:cus_count="2"  不加直接使用ohos:cus_count="2"
 * <p>
 */
public class AttrUtils {
    /**
     * getStringFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return String
     */
    public static String getStringFromAttr(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getDimensionFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Integer
     */
    public static Integer getDimensionFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getIntegerFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Integer
     */
    public static Integer getIntegerFromAttr(AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getFloatFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return float
     */
    public static float getFloatFromAttr(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getLongFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return Long
     */
    public static Long getLongFromAttr(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getColorFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return int
     */
    public static int getColorFromAttr(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getBooleanFromAttr.
     *
     * @param attrSet      attrSet
     * @param name         name
     * @param defaultValue defaultValue
     * @return boolean
     */
    public static boolean getBooleanFromAttr(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            return value;
        }
        return value;
    }

    /**
     * getElementFromAttr.
     *
     * @param attrSet attrSet
     * @param name    name
     * @return Element
     */
    public static Element getElementFromAttr(AttrSet attrSet, String name) {
        Element value = null;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getElement();
            }
        } catch (Exception e) {
            return null;
        }
        return value;
    }

}
