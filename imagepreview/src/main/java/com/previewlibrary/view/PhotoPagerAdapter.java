package com.previewlibrary.view;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * PhotoPagerAdapter.
 *
 * @author author
 * @version version
 */
public class PhotoPagerAdapter extends PageSliderProvider {
    private Context mContext;

    private List<BasePhotoFragment> mItems = new ArrayList<>();

    public PhotoPagerAdapter(Context context) {
        this.mContext = context;
    }

    public void setItems(List<BasePhotoFragment> mItems) {
        this.mItems = mItems;
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        // PreviewItemFragment
        BasePhotoFragment basePhotoFragment = mItems.get(i);
        return basePhotoFragment.attachToContainer(componentContainer);
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return component == o;
    }
}
