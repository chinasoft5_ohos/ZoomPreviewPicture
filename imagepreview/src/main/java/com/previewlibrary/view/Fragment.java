/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.previewlibrary.view;

import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.utils.PacMap;

/**
 * Fragment.
 *
 * @author author
 * @version version
 */
public abstract class Fragment implements Component.BindStateChangedListener {
    /**
     * mContext.
     */
    protected Context mContext;
    /**
     * mContext.
     */
    protected StackLayout mRootView;
    /**
     * mChangeFragment.
     */
    protected ChangeFragmentTool mChangeFragment;
    /**
     * arguments.
     */
    protected PacMap arguments;
    /**
     * mPageSlider.
     */
    protected PageSlider mPageSlider;

    /**
     * setPageSlider.
     *
     * @param pageSlider pageSlider
     */
    public void setPageSlider(PageSlider pageSlider) {
        this.mPageSlider = pageSlider;
    }

    /**
     * setArguments.
     *
     * @param pacMap pacMap
     */
    public void setArguments(PacMap pacMap) {
        this.arguments = pacMap;
    }

    /**
     * getArguments.
     *
     * @return PacMap
     */
    public PacMap getArguments() {
        return arguments;
    }

    /**
     * setChangeFragmentTool.
     *
     * @param changeFragment changeFragment
     */
    public void setChangeFragmentTool(ChangeFragmentTool changeFragment) {
        this.mChangeFragment = changeFragment;
    }

    /**
     * Fragment.
     *
     * @param context context
     */
    public Fragment(Context context) {
        this.mContext = context;
    }

    /**
     * onCreateView.
     *
     * @param layoutScatter layoutScatter
     * @param container     container
     * @return Component
     */
    protected abstract Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container);

    /**
     * onViewCreate.
     */
    protected abstract void onViewCreate();

    /**
     * findComponentById.
     *
     * @param id id
     * @return Component
     */
    public Component findComponentById(int id) {
        return mRootView.findComponentById(id);
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
        Component createView = onCreateView(LayoutScatter.getInstance(mContext), mRootView);
        mRootView.addComponent(createView);
        onViewCreate();
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        onDestroyView();
    }

    /**
     * attachToContainer.
     *
     * @param componentContainer componentContainer
     * @return Component
     */
    public Component attachToContainer(ComponentContainer componentContainer) {
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        this.mRootView = new StackLayout(mContext);
        mRootView.setLayoutConfig(layoutConfig);
        mRootView.setBindStateChangedListener(this);
        componentContainer.addComponent(mRootView);
        return mRootView;
    }

    /**
     * onForeground.
     *
     * @param intent intent
     */
    public void onForeground(Intent intent) {

    }

    /**
     * onBackground.
     */
    public void onBackground() {
    }

    /**
     * onDestroyView.
     */
    public void onDestroyView() {
    }

    /**
     * ChangeFragmentTool.
     */
    public interface ChangeFragmentTool {
        /**
         * changeFragment.
         *
         * @param index index
         */
        void changeFragment(int index);

        /**
         * getCurrentPage.
         *
         * @return int
         */
        int getCurrentPage();
    }


}
