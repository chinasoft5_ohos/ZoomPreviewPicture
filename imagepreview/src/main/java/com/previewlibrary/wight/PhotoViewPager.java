package com.previewlibrary.wight;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.app.Context;

/**
 * @author yangc
 * date 2017/4/26
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated:
 * https://github.com/chrisbanes/PhotoView
 * Issues With ViewGroups
 */
public class PhotoViewPager extends PageSlider {
    public PhotoViewPager(Context context) {
        super(context);
    }

    public PhotoViewPager(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public PhotoViewPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
