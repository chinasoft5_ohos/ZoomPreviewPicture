package com.previewlibrary;


import com.previewlibrary.enitity.IThumbViewInfo;
import com.previewlibrary.loader.VideoClickListener;
import com.previewlibrary.view.BasePhotoFragment;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.utils.Sequenceable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yangc on 2017/9/12.
 * E-Mail:yangchaojiang@outlook.com
 * Deprecated:
 */
public final class GPreviewBuilder {
    private Ability mContext;
    private Intent intent;
    private Class className;
    private VideoClickListener videoClickListener;

    private GPreviewBuilder(Ability ability) {
        mContext = ability;
        intent = new Intent();
    }

    /**
     * 设置开始启动预览.
     *
     * @param ability 启动
     * @return GPreviewBuilder
     **/
    public static GPreviewBuilder from(Ability ability) {
        return new GPreviewBuilder(ability);
    }

    /**
     * 设置开始启动预览.
     *
     * @param fragment 启动
     * @return GPreviewBuilder
     **/
    public static GPreviewBuilder from(Fraction fragment) {
        return new GPreviewBuilder(fragment.getFractionAbility());
    }

    /**
     * 自定义预览Ability 类名.
     *
     * @param className 继承GPreviewAbility
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder to(Class className) {
        this.className = className;
        return this;
    }

    /**
     * 自定义预览ability 类名  方便自定义数据.
     *
     * @param className 继承GPreviewAbility
     * @param bundle    需携带的参数
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder to(Class className, IntentParams bundle) {
        this.className = className;
        intent.setParams(bundle);
        return this;
    }

    /**
     * 设置数据源.
     *
     * @param imgUrls 数据
     * @param <T>     你的实体类类型
     * @return GPreviewBuilder
     **/
    public <T extends IThumbViewInfo> GPreviewBuilder setData(List<T> imgUrls) {
        intent.setSequenceableArrayListParam("imagePaths", new ArrayList<Sequenceable>(imgUrls));
        return this;
    }

    /**
     * 设置单个数据源.
     *
     * @param imgUrl 数据
     * @param <E>    你的实体类类型
     * @return GPreviewBuilder
     **/
    public <E extends IThumbViewInfo> GPreviewBuilder setSingleData(E imgUrl) {
        ArrayList arrayList = new ArrayList<Sequenceable>();
        arrayList.add(imgUrl);
        intent.setSequenceableArrayListParam("imagePaths", arrayList);
        return this;
    }

    /**
     * 设置数据源.
     *
     * @param className 你的Fragment类
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setUserFragment(Class<? extends BasePhotoFragment> className) {
        intent.setParam("className", className);
        return this;
    }

    /**
     * 设置默认索引.
     *
     * @param currentIndex 数据
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setCurrentIndex(int currentIndex) {
        intent.setParam("position", currentIndex);
        return this;
    }

    /**
     * 设置指示器类型.
     *
     * @param indicatorType 枚举
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setType(IndicatorType indicatorType) {
        intent.setParam("type", indicatorType);
        return this;
    }

    /**
     * 设置图片禁用拖拽返回.
     *
     * @param isDrag true  可以 false 默认 true
     * @return GPreviewBuilder
     * @deprecated use {@link #isDisableDrag(boolean) }
     **/
    public GPreviewBuilder setDrag(boolean isDrag) {
        intent.setParam("isDrag", isDrag);
        return this;
    }

    /**
     * 设置图片禁用拖拽返回.
     *
     * @param isDrag true  可以 false 默认 true
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder isDisableDrag(boolean isDrag) {
        intent.setParam("isDrag", isDrag);
        return this;
    }

    /**
     * 设置图片禁用拖拽返回.
     *
     * @param isDrag      true  可以 false 默认 true
     * @param sensitivity sensitivity MAX_TRANS_SCALE 的值来控制灵敏度。
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder isDisableDrag(boolean isDrag, float sensitivity) {
        intent.setParam("isDrag", isDrag);
        intent.setParam("sensitivity", sensitivity);
        return this;
    }

    /**
     * 设置图片禁用拖拽返回.
     *
     * @param isDrag      true  可以 false 默认 true
     * @param sensitivity sensitivity MAX_TRANS_SCALE 的值来控制灵敏度。
     * @return GPreviewBuilder
     * @deprecated use {@link #isDisableDrag(boolean, float) }
     **/
    public GPreviewBuilder setDrag(boolean isDrag, float sensitivity) {
        intent.setParam("isDrag", isDrag);
        intent.setParam("sensitivity", sensitivity);
        return this;
    }

    /**
     * 是否设置为一张图片时 显示指示器  默认显示
     *
     * @param isShow true  显示 false 不显示
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setSingleShowType(boolean isShow) {
        intent.setParam("isShow", isShow);
        return this;
    }

    /**
     * 设置超出内容点击退出（黑色区域）.
     *
     * @param isSingleFling true  可以 false
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setSingleFling(boolean isSingleFling) {
        intent.setParam("isSingleFling", isSingleFling);
        return this;
    }

    /**
     * 设置动画的时长.
     *
     * @param setDuration 单位毫秒
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setDuration(int setDuration) {
        intent.setParam("duration", setDuration);
        return this;
    }

    /**
     * 设置是否全屏.
     *
     * @param isFullscreen 单位毫秒
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setFullscreen(boolean isFullscreen) {
        intent.setParam("isFullscreen", isFullscreen);
        return this;
    }

    /**
     * 设置只有图片没有放大或者的缩小状态触退出.
     *
     * @param isScale true false
     * @return GPreviewBuilder
     **/
    public GPreviewBuilder setIsScale(boolean isScale) {
        intent.setParam("isScale", isScale);
        return this;
    }

    /**
     * 设置是视频点击播放回调.
     *
     * @param listener listener
     * @return GPreviewBuilder GPreviewBuilder
     **/
    public GPreviewBuilder setOnVideoPlayerListener(VideoClickListener listener) {
        this.videoClickListener = listener;
        return this;
    }

    /**
     * 启动.
     **/
    public void start() {
        Intent.OperationBuilder operationBuilder = new Intent
                .OperationBuilder();
        operationBuilder.withBundleName(mContext.getBundleName());
        if (className == null) {
            operationBuilder.withAbilityName(GPreviewAbility.class.getCanonicalName());
        } else {
            operationBuilder.withAbilityName(className.getCanonicalName());
        }
        intent.setOperation(operationBuilder.build());
        BasePhotoFragment.setListener(videoClickListener);
        mContext.startAbility(intent);
        mContext.setTransitionAnimation(0, 0);
    }


    /**
     * 指示器类型.
     **/
    public enum IndicatorType {
        Dot, Number
    }
}
